<?php
//creación de la clase carro
class Carro2{
	//declaracion de propiedades
	public $color;
	public $modelo;
	public $anio;

	private $resultadoVerificacion;

	//declaracion del método verificación
	public function verificacion(){
		if(!isset($this->anio)){
			$this->resultadoVerificacion = '';
		} else if($this->anio < 1990){
			$this->resultadoVerificacion = 'No';
		} else if($this->anio >= 1990 and $this->anio <= 2010){
			$this->resultadoVerificacion = 'Revisión';
		} else{
			$this->resultadoVerificacion = 'Sí';
		}
	}

	public function getResultadoVerificacion(){
		return $this->resultadoVerificacion;
	}

}

//creación de instancia a la clase Carro
$Carro1 = new Carro2();

if (!empty($_POST)){
	$Carro1->color=$_POST['color'];
	$Carro1->modelo=$_POST['modelo'];
	$Carro1->anio=date('Y', strtotime($_POST['anio']));
	$Carro1->verificacion();
}




