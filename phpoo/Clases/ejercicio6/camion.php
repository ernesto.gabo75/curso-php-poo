<?php
    require_once('transporte.php');

    class Camion extends transporte{
        private $tipo_carga;
        private $no_ejes;

        public function __construct($nom, $vel, $com, $tipo_carga, $no_ejes){
            parent::__construct($nom, $vel, $com);
            $this->tipo_carga = $tipo_carga;
            $this->no_ejes = $no_ejes;
        }

        public function resumenCamion(){
            $mensaje = parent::crear_ficha();
            $mensaje .= '<tr>
                            <td>Tipo de carga:</td>
                            <td>'. $this->tipo_carga.'</td>				
                        </tr>
                        <tr>
                            <td>Número de ejes:</td>
                            <td>'. $this->no_ejes.'</td>
                        </tr>';

            return $mensaje;
        }
    };
?>