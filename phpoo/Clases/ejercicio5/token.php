<?php  
//declaracion de clase token
	class token{
		//declaracion de atributos
		private $nombre;
		private $token;
		//declaracion de metodo constructor
		public function __construct($nombre_front){
			$this->nombre=$nombre_front;
			$this->token=$this->generateToken();
		}

		private function generateToken(){
			define('SIMBOLOS', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ');
			define('MAX_PASSWORD', 4);
			$simbolos_len = strlen(SIMBOLOS);
			$token = '';
			for ($i=0; $i < MAX_PASSWORD; $i++) { 
				$index = random_int(0, $simbolos_len - 1);
				$token .= SIMBOLOS[$index];
			}
			
			return $token;
		}

		//declaracion del metodo mostrar para armar el mensaje con el nombre y token
		public function mostrar(){
			return 'Hola '.$this->nombre.' este es tu token: '.$this->token;
		}

		//declaracion de metodo destructor
		public function __destruct(){
			//destruye token
			$this->token='El token ha sido destruido';
			echo $this->token;
		}
	}

$mensaje='';


if (!empty($_POST)){
	//creacion de objeto de la clase
	$token1= new token($_POST['nombre']);
	$mensaje=$token1->mostrar();
}


?>
